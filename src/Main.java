
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import net.miginfocom.swing.MigLayout;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.jdom.output.XMLOutputter;
import java.util.Enumeration;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
//import javax.management.relation.Role;
import javax.swing.border.Border;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.jdom.output.Format;
//import javax.swing.tree.TreePath;
//import net.miginfocom.layout.LC;

/**
 *
 * @author Makik
 */
public class Main implements TreeSelectionListener {

    public static List<String> nazvyrole = new ArrayList();
    public static List<String> nazvyukoly = new ArrayList();
    public static List<List<String>> nazvypodukoly = new ArrayList();
    public static List<List<List<List<String>>>> nazvyBP = new ArrayList();
    final static JPanel panel2 = new JPanel();
    final static JFrame mojeOkno = new JFrame("Přiřazení BP k Open UP");
    private static List<ButtonGroup> buttonGroups = new ArrayList();
    public static String fileName = "D:\\Programovani\\priklady\\MarkiDP\\list.xml";
    public static List<Integer> hod = new ArrayList<Integer>(); 

    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0);

        return nValue.getNodeValue();
    }

    public static void main(String[] args) {


        mojeOkno.setPreferredSize(new Dimension(1000, 750));
        mojeOkno.getContentPane().setLayout(new MigLayout());
        JPanel panel1 = new JPanel();
        panel1.setPreferredSize(new Dimension(850, 1000));
        panel1.setBorder(BorderFactory.createTitledBorder("Role a úkoly"));
        panel1.setLayout(new BorderLayout());
        String titletext = new String();

        DefaultMutableTreeNode Role = new DefaultMutableTreeNode("Role");

        try {

            File fXmlFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList tmpNL = doc.getElementsByTagName("SETTING");

            titletext = getTagValue("TITLETEXT", (Element) tmpNL.item(0));

            NodeList SeznamRoli = doc.getElementsByTagName("ROLE");
            for (int i = 0; i < SeznamRoli.getLength(); i++) {
                Node XMLRole = SeznamRoli.item(i);

                if (XMLRole.getNodeType() == Node.ELEMENT_NODE) {
                    Element e = (Element) XMLRole;

                    DefaultMutableTreeNode Rolenazev = new DefaultMutableTreeNode(getTagValue("NAME", e));
                    NodeList SeznamUkolu = e.getElementsByTagName("UKOLY");

                    for (int j = 0; j < SeznamUkolu.getLength(); j++) {
                        Node SUkoly = SeznamUkolu.item(j);
                        Element e2 = (Element) SUkoly;
                        NodeList Seznam = e2.getElementsByTagName("UKOL");
                        for (int k = 0; k < Seznam.getLength(); k++) {
                            Node Ukol = Seznam.item(k);
                            Element e3 = (Element) Ukol;
                            DefaultMutableTreeNode ukolynazev = new DefaultMutableTreeNode(getTagValue("NAME", e3));
                            nazvyukoly.add(getTagValue("NAME", e3));
                            Rolenazev.add(ukolynazev);
                            List<List<List<String>>> tmp = new ArrayList();
                            NodeList Seznampodkolu = e3.getElementsByTagName("PODUKOLY");
                            for (int l = 0; l < Seznampodkolu.getLength(); l++) {
                                Node podUkoly = Seznampodkolu.item(l);
                                Element e4 = (Element) podUkoly;
                                NodeList Seznampod = e4.getElementsByTagName("PODUKOL");
                                List<String> pmt = new ArrayList();
                                for (int m = 0; m < Seznampod.getLength(); m++) {
                                    Node podUkol = Seznampod.item(m);
                                    Element e5 = (Element) podUkol;
                                    DefaultMutableTreeNode podukolynazev = new DefaultMutableTreeNode(getTagValue("NAME", e5));
                                    pmt.add(getTagValue("NAME", e5));
                                    ukolynazev.add(podukolynazev);

                                    NodeList SeznamBPS = e5.getElementsByTagName("BPS");
                                    List<List<String>> tmp2 = new ArrayList();
                                    for (int n = 0; n < SeznamBPS.getLength(); n++) {
                                        Node BPS = SeznamBPS.item(n);
                                        Element e6 = (Element) BPS;
                                        NodeList SeznamBP = e6.getElementsByTagName("BP");


                                        for (int o = 0; o < SeznamBP.getLength(); o++) {
                                            Node BP = SeznamBP.item(o);
                                            Element e7 = (Element) BP;

                                            //e7.setTextContent(hod.item);

                                            List<String> tmp3 = new ArrayList();
                                            tmp3.add(getTagValue("TEXT", e7));
                                            tmp3.add(getTagValue("HODNOCENI", e7));
                                            tmp2.add(tmp3);
                                        }


                                    }
                                    tmp.add(tmp2);
                                }
                                nazvypodukoly.add(pmt);
                            }
                            nazvyBP.add(tmp);
                        }
                    }
                    Role.add(Rolenazev);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        JTree tree = new JTree(Role);
        panel1.add(tree);



        panel2.setLayout(new BorderLayout());
        panel2.setPreferredSize(new Dimension(650, 750));

        JTextArea tmpTA = new JTextArea(titletext);
        tmpTA.setEditable(false);
        panel2.add(tmpTA);

        MouseAdapter ma = new MouseAdapter() {

            private void myPopupEvent(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                JTree tree = (JTree) e.getSource();
                TreePath path = tree.getPathForLocation(x, y);
                if (path == null) {
                    return;
                }

                tree.setSelectionPath(path);

                JPopupMenu popup = new JPopupMenu();
                JMenuItem findMenuItem = new JMenuItem("Find");
                ActionListener actionListener = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        System.out.println("Selected: " + actionEvent.getActionCommand());
                    }
                };
                findMenuItem.addActionListener(actionListener);
                popup.add(findMenuItem);
                popup.show(tree, x, y);

            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    myPopupEvent(e);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    myPopupEvent(e);
                }
            }
        };
        
        tree.addMouseListener(ma);
        tree.addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent tse) {
                JTree tree1 = (JTree) tse.getSource();
                DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree1.getLastSelectedPathComponent();
                String selectedNodeName = selectedNode.toString();

                panel2.removeAll();
                if (selectedNode.isLeaf()) {
                    int index = nazvyukoly.indexOf(selectedNode.getParent().toString());
                    List<List<List<String>>> tmp = nazvyBP.get(index);

                    List<List<String>> tmp2 = tmp.get(nazvypodukoly.get(index).indexOf(selectedNodeName));

                    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(panel2);
                    panel2.setLayout(jPanel2Layout);

                    JTextArea popisBP = new JTextArea();;



                    ParallelGroup horGroup = jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
                    SequentialGroup horSeqGroup = jPanel2Layout.createSequentialGroup();
                    ParallelGroup sloupec1 = jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);

                    ParallelGroup sloupec2 = jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);

                    ParallelGroup verGroup = jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING);
                    SequentialGroup verSeqGroup = jPanel2Layout.createSequentialGroup();
                    verSeqGroup.addGap(35, 35, 35);
                    //prochazeni jednotlivych BP pro rozkliknuty podukol
                    for (int i = 0; i < tmp2.size(); i++) {

                        List<String> tmp3 = tmp2.get(i);
                        //nazev
                        popisBP = new JTextArea(tmp3.get(0));
                        popisBP.setPreferredSize(new Dimension(5, 5));
                        ButtonGroup buttonGroup1 = new ButtonGroup();

                        JRadioButton jRadioButton1 = new JRadioButton();
                        JRadioButton jRadioButton2 = new javax.swing.JRadioButton();
                        JRadioButton jRadioButton3 = new javax.swing.JRadioButton();
                        JRadioButton jRadioButton4 = new javax.swing.JRadioButton();
                        

                        ActionListener sliceActionListener = new ActionListener() {

                            public void actionPerformed(ActionEvent actionEvent) {
                                //List<String> hodstring = new ArrayList<String>(hod.size());
                                for (int i = 0; i < buttonGroups.size(); i++) {
                                    ButtonGroup tmp = buttonGroups.get(i);
                                    Boolean found = false;
                                    for (Enumeration e = tmp.getElements(); e.hasMoreElements();) {
                                        JRadioButton b = (JRadioButton) e.nextElement();
                                        if (b.getModel() == tmp.getSelection()) {
                                            //System.out.println("Selected: " + b.getText());
                                            if ("Full".equals(b.getText())) {
                                                hod.add(100);
                                                found = true;
                                                //System.out.println(b.getText());
                                            } else if ("Half".equals(b.getText())) {
                                                hod.add(50);
                                                found = true;
                                                //System.out.println(b.getText());
                                            } else if ("Low".equals(b.getText())) {
                                                hod.add(25);
                                                found = true;
                                                //System.out.println(b.getText());
                                            } else {
                                                hod.add(0);
                                                found = true;
                                                //System.out.println(b.getText());
                                            }

                                        }
                                        //System.out.println("SIZE:" + hod.size());
                                    }
                                    if (!found) {
                                        hod.add(0);
                                        //System.out.println(b.getText());
                                    }
                                    found = false;
                                }

                                try {
                                    System.out.println(fileName);
                                    File fXmlFile = new File(fileName);
                                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                                    Document doc = dBuilder.parse(fXmlFile);
                                    doc.getDocumentElement().normalize();
                                    //NodeList tmpNL = doc.getElementsByTagName("SETTING");
                                    //System.out.println(hod.toString());
                                    NodeList SeznamRoli = doc.getElementsByTagName("ROLE");
                                    for (int i = 0; i < SeznamRoli.getLength(); i++) {
                                        Node XMLRole = SeznamRoli.item(i);

                                        if (XMLRole.getNodeType() == Node.ELEMENT_NODE) {
                                            Element e = (Element) XMLRole;

                                            NodeList SeznamUkolu = e.getElementsByTagName("UKOLY");

                                            for (int j = 0; j < SeznamUkolu.getLength(); j++) {
                                                Node SUkoly = SeznamUkolu.item(j);
                                                Element e2 = (Element) SUkoly;
                                                NodeList Seznam = e2.getElementsByTagName("UKOL");
                                                for (int k = 0; k < Seznam.getLength(); k++) {
                                                    Node Ukol = Seznam.item(k);
                                                    Element e3 = (Element) Ukol;
//                                                    List<List<List<String>>> tmp = new ArrayList();
                                                    NodeList Seznampodkolu = e3.getElementsByTagName("PODUKOLY");

                                                    for (int l = 0; l < Seznampodkolu.getLength(); l++) {
                                                        Node podUkoly = Seznampodkolu.item(l);
                                                        Element e4 = (Element) podUkoly;
                                                        NodeList Seznampod = e4.getElementsByTagName("PODUKOL");
                                                        List<String> pmt = new ArrayList();

                                                        for (int m = 0; m < Seznampod.getLength(); m++) {
                                                            Node podUkol = Seznampod.item(m);
                                                            Element e5 = (Element) podUkol;
                                                            pmt.add(getTagValue("NAME", e5));

                                                            NodeList SeznamBPS = e5.getElementsByTagName("BPS");
                                                            //List<List<String>> tmp2 = new ArrayList();

                                                            for (int n = 0; n < SeznamBPS.getLength(); n++) {
                                                                Node BPS = SeznamBPS.item(n);
                                                                Element e6 = (Element) BPS;
                                                                NodeList SeznamBP = e6.getElementsByTagName("BP");


                                                                for (int o = 0; o < SeznamBP.getLength(); o++) {
                                                                    Node BP = SeznamBP.item(o);
                                                                    Element e7 = (Element) BP;

                                                                    NodeList SeznamHodnoceniBP = e7.getElementsByTagName("HODNOCENI");

                                                                    for (int p = 0; p < SeznamHodnoceniBP.getLength(); p++) {
                                                                        Node HodBP = SeznamHodnoceniBP.item(p);
                                                                        Element e8 = (Element) HodBP;
                                                                        System.out.println(hod.get(p).toString());
                                                                        e8.setTextContent(hod.get(o).toString());
                                                                    }
                                                                }


                                                            }
                                                            //    tmp.add(tmp2);
                                                        }
                                                        //   nazvypodukoly.add(pmt);
                                                    }
                                                    //  nazvyBP.add(tmp);
                                                }
                                            }
                                            //Role.add(Rolenazev);
                                        }
                                    }
                                    //setting up a transformer
                                    TransformerFactory transfac = TransformerFactory.newInstance();
                                    Transformer trans = transfac.newTransformer();

                                    //generating string from xml tree
                                    StringWriter sw = new StringWriter();
                                    StreamResult result = new StreamResult(sw);
                                    DOMSource source = new DOMSource(doc);
                                    trans.transform(source, result);
                                    String xmlString = sw.toString();

                                    //Saving the XML content to File
                                    OutputStream f0;
                                    byte buf[] = xmlString.getBytes();
                                    f0 = new FileOutputStream(fileName);
                                    for (int i = 0; i < buf.length; i++) {
                                        f0.write(buf[i]);
                                    }
                                    f0.close();
                                    buf = null;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        };


                        buttonGroup1.add(jRadioButton1);
                        jRadioButton1.setText("Full");
                        jRadioButton1.addActionListener(sliceActionListener);
                        if ("100".equals(tmp3.get(1))) {
                            System.out.println("AA: 100");
                           jRadioButton1.setSelected(true); 
                        }

                        buttonGroup1.add(jRadioButton2);
                        jRadioButton2.setText("Half");
                        jRadioButton2.addActionListener(sliceActionListener);
                        if ("50".equals(tmp3.get(1))) {
                           jRadioButton2.setSelected(true); 
                        }


                        buttonGroup1.add(jRadioButton3);
                        jRadioButton3.setText("Low");
                        jRadioButton3.addActionListener(sliceActionListener);
                        if ("25".equals(tmp3.get(1))) {
                           jRadioButton3.setSelected(true); 
                        }


                        buttonGroup1.add(jRadioButton4);
                        jRadioButton4.setText("Null");
                        jRadioButton4.addActionListener(sliceActionListener);
                        if ("0".equals(tmp3.get(1))) {
                           jRadioButton4.setSelected(true);
                           popisBP.setForeground(Color.red);
                        }
                        System.out.println(tmp3.get(1));


                        buttonGroups.add(buttonGroup1);

                        
                        sloupec1.addComponent(popisBP, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE);
                        SequentialGroup buttonSHGroup = (jPanel2Layout.createSequentialGroup().addComponent(jRadioButton1).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jRadioButton2).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jRadioButton3).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jRadioButton4));

                        sloupec2.addGroup(buttonSHGroup);

                        ParallelGroup radek = jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER);
                        radek.addComponent(popisBP, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE);
                        radek.addComponent(jRadioButton1);
                        radek.addComponent(jRadioButton2);
                        radek.addComponent(jRadioButton3);
                        radek.addComponent(jRadioButton4);
                        verSeqGroup.addGroup(radek);


                        //popisBP.setBorder(BorderFactory.createLineBorder(Color.BLACK));

                        //System.out.println("pridavam");

                        // hodnoceni
                        //System.out.println(tmp3.get(1));
                    }

                    horSeqGroup.addGroup(sloupec1);
                    horSeqGroup.addGroup(sloupec2);
                    horSeqGroup.addGap(35, 35, 35);


                    horGroup.addGroup(horSeqGroup);
                    jPanel2Layout.setHorizontalGroup(horGroup);

                    verGroup.addGroup(verSeqGroup);
                    jPanel2Layout.setVerticalGroup(verGroup);

                    //System.out.println(selectedNodeName);


                } else {
                    JLabel qwer = new JLabel(selectedNodeName);
                    panel2.add(qwer);
                }
                mojeOkno.pack();
            }
        });


        JTitlePanel panelSPopiskem = new JTitlePanel("Role", null, panel1, null);
        mojeOkno.getContentPane().add(panelSPopiskem);
        mojeOkno.add(new JScrollPane(panelSPopiskem)).setPreferredSize(new Dimension(550, 750));


        JTitlePanel panelspopisem2 = new JTitlePanel("Open UP", null, panel2, null);
        mojeOkno.getContentPane().add(panelspopisem2);
        panelspopisem2.setPreferredSize(new Dimension(850, 750));

        mojeOkno.pack();


        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(new PlasticXPLookAndFeel());
                    SwingUtilities.updateComponentTreeUI(mojeOkno);
                } catch (UnsupportedLookAndFeelException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        Runnable doRun = new Runnable() {

            @Override
            public void run() {
                mojeOkno.setVisible(true);
            }
        };
        SwingUtilities.invokeLater(doRun);


    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
